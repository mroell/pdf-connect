//
//  PDFModel.h
//  PDF Connect
//
//  Created by Matthias Roell on 18.09.13.
//  Copyright (c) 2013 Matthias Roell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Quartz/Quartz.h>

@interface PDFModel : NSObject <NSTableViewDataSource, NSTableViewDelegate, NSPopoverDelegate>
{
    NSTableView * _table;
    PDFDocument * _document;
    
    NSThread * _thread;
    NSLock * _lock;
}

@property (nonatomic, strong) NSMutableArray * documents;

+ (PDFModel *)sharedModel;

- (void)setTable:(NSTableView *)table;
- (NSTableView *)table;

- (void)newDocument;
- (PDFDocument *)document;

- (void)addDocumentsObject:(PDFDocument *)object;
- (void)addDocuments:(NSSet *)objects;
- (void)removeDocumentsObject:(PDFDocument *)object;
- (void)removeObjectFromDocumentsAtIndex:(NSUInteger)index;
- (void)insertObject:(PDFDocument *)object inDocumentsAtIndex:(NSUInteger)index;
- (void)insertDocuments:(NSArray *)array atIndexes:(NSIndexSet *)indexes;

- (IBAction)editButtonAction:(NSButton *)sender;
- (IBAction)pwButtonAction:(NSButton *)sender;

@end

extern NSString * PDFModelDocumentUpdatedNotification;