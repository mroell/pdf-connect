//
//  NSTextField+Shake.m
//  PDF Connect
//
//  Created by Matthias Röll on 24.09.13.
//  Copyright (c) 2013 Matthias Roell. All rights reserved.
//
// Code from http://www.cimgf.com/2008/02/27/core-animation-tutorial-window-shake-effect/

#import "NSTextField+Shake.h"

@implementation NSTextField (Shake)

- (CAKeyframeAnimation *)shakeAnimation:(NSRect)frame
{
    static int numberOfShakes = 3;
    static float durationOfShake = 0.5f;
    static float vigourOfShake = 0.01f;
    
    CAKeyframeAnimation *shakeAnimation = [CAKeyframeAnimation animation];
    
    CGMutablePathRef shakePath = CGPathCreateMutable();
    CGPathMoveToPoint(shakePath, NULL, NSMinX(frame), NSMinY(frame));
    int index;
    for (index = 0; index < numberOfShakes; ++index)
    {
        CGPathAddLineToPoint(shakePath, NULL, NSMinX(frame) - frame.size.width * vigourOfShake, NSMinY(frame));
        CGPathAddLineToPoint(shakePath, NULL, NSMinX(frame) + frame.size.width * vigourOfShake, NSMinY(frame));
    }
    CGPathCloseSubpath(shakePath);
    shakeAnimation.path = shakePath;
    shakeAnimation.duration = durationOfShake;
    return shakeAnimation;
}

@end
