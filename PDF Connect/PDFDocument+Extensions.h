//
//  PDFDocument+Extensions.h
//  PDF Connect
//
//  Created by Matthias Roell on 18.09.13.
//  Copyright (c) 2013 Matthias Roell. All rights reserved.
//

#include <Foundation/Foundation.h>
#include <Quartz/Quartz.h>

@interface PDFDocument (Extensions)

- (NSString *)documentName;

@end
