//
//  PDFModel.m
//  PDF Connect
//
//  Created by Matthias Roell on 18.09.13.
//  Copyright (c) 2013 Matthias Roell. All rights reserved.
//

#import "PDFModel.h"

#import "PDFDocument+Extensions.h"
#import "NSTextField+Shake.h"
#import "AppDelegate.h"

NSString * PDFModelDocumentUpdatedNotification = @"PDFModelDocumentUpdatedNotification";

@implementation PDFModel

static PDFModel * _sharedPDFModel = nil;

+ (PDFModel *)sharedModel
{
	@synchronized([PDFModel class])
	{
		if (!_sharedPDFModel)
		{
			_sharedPDFModel = [[self alloc] init];
		}
		
		return _sharedPDFModel;
	}
	
	return nil;
}

+ (id)alloc
{
	@synchronized([PDFModel class])
	{
		NSAssert(_sharedPDFModel == nil, @"Attempted to allocate a second instance of a singleton.");
		_sharedPDFModel = [super alloc];
		return _sharedPDFModel;
	}
	
	return nil;
}

- (id)init
{
    self = [super init];
    
    if (self)
    {
        _document = [[PDFDocument alloc] init];
    }
    
    return self;
}

#pragma mark *** Private ***

- (void)_generateDocument
{
    if (!_lock)
    {
        _lock = [[NSLock alloc] init];
    }
    
    [_lock lock];
    
    if ([_documents count] == 1)
    {
        _document = [_documents lastObject];
    }
    else
    {
        _document = [[PDFDocument alloc] init];
        
        for (NSUInteger i = 0; i < self.documents.count; i++)
        {
            PDFDocument * currentDocument = [self.documents objectAtIndex:i];
            
            for (NSUInteger j = 0; j < [currentDocument pageCount]; j++)
            {
                [_document insertPage:[currentDocument pageAtIndex:j] atIndex:[_document pageCount]];
            }
        }
    }
    
    [_lock unlock];
    
    [[NSNotificationCenter defaultCenter] performSelectorOnMainThread:@selector(postNotificationName:object:)
                                                           withObject:PDFModelDocumentUpdatedNotification
                                                        waitUntilDone:NO];
}

- (void)_documentChangedNotification
{
    if (!_thread || [_thread isFinished])
    {
        _thread = [[NSThread alloc] initWithTarget:self selector:@selector(_generateDocument) object:nil];
        [_thread setName:@"Document Building Thread"];
    }
    else if ([_thread isExecuting])
    {
        [_thread cancel];
        
        _thread = [[NSThread alloc] initWithTarget:self selector:@selector(_generateDocument) object:nil];
        [_thread setName:@"Document Building Thread"];
    }
    
    [_thread start];
}

#pragma mark *** Properties ***

- (NSMutableArray *)documents
{
    if (!_documents)
    {
        _documents = [@[] mutableCopy];
    }
    
    return _documents;
}

- (void)setTable:(NSTableView *)table
{
    static NSArray * draggingTypes;
    
    if (!draggingTypes)
    {
        draggingTypes = @[NSFilenamesPboardType];
    }
    
    _table = table;
    
    [_table setDataSource:self];
    [_table setDelegate:self];
    
    [_table registerForDraggedTypes:draggingTypes];
    [_table setDraggingSourceOperationMask:NSDragOperationCopy forLocal:NO];
}
- (NSTableView *)table
{
    return _table;
}


#pragma mark *** Documents Array ***

- (void)newDocument
{
    [self.documents removeAllObjects];
    
    [self _documentChangedNotification];
    [self.table reloadData];
}
- (PDFDocument *)document
{
    if (!_lock)
    {
        _lock = [[NSLock alloc] init];
    }
    
    [_lock lock];
    [_lock unlock];
    
    return _document;
}

- (void)addDocumentsObject:(PDFDocument *)object
{
    [self.documents addObject:object];
    
    [self _documentChangedNotification];
    [self.table reloadData];
}

- (void)addDocuments:(NSSet *)objects
{
    for (id document in objects)
    {
        if ([[document class] isSubclassOfClass:[PDFDocument class]])
        {
            [self.documents addObject:document];
        }
    }
    
    [self _documentChangedNotification];
    [self.table reloadData];
}

- (void)removeDocumentsObject:(PDFDocument *)object
{
    [self removeDocumentsObject:object];
    
    [self _documentChangedNotification];
    [self.table reloadData];
}
- (void)removeObjectFromDocumentsAtIndex:(NSUInteger)index
{
    [self.documents removeObjectAtIndex:index];
    
    [self _documentChangedNotification];
    [self.table reloadData];
}

- (void)insertObject:(PDFDocument *)object inDocumentsAtIndex:(NSUInteger)index
{
    [self.documents insertObject:object atIndex:index];
    
    [self _documentChangedNotification];
    [self.table reloadData];
}

- (void)insertDocuments:(NSArray *)array atIndexes:(NSIndexSet *)indexes
{
    BOOL arrayOK = YES;
    
    for (id document in array)
    {
        if (![[document class] isSubclassOfClass:[PDFDocument class]])
        {
            arrayOK = NO;
            break;
        }
    }
    
    if (arrayOK)
    {
        [_documents insertObjects:array atIndexes:indexes];
        
        [self _documentChangedNotification];
        [self.table reloadData];
    }
}

#pragma mark *** Edit Action ***

- (void)editButtonAction:(NSButton *)sender
{
    NSInteger index = [self.table rowForView:sender];
    NSView * editView = [(AppDelegate *)[NSApp delegate] editView];
    NSPopover * popover = [(AppDelegate *)[NSApp delegate] editPopover];
    
    PDFDocument * document = [self.documents objectAtIndex:index];
    
    [self.table selectRowIndexes:[NSIndexSet indexSetWithIndex:index] byExtendingSelection:NO];
    
    for (NSView * sub in [editView subviews])
    {
        if ([sub.identifier isEqualToString:@"documentName"])
        {
            [(NSTextField *)sub setStringValue:document.documentName];
        }
        else if ([sub.identifier isEqualToString:@"passwordField"])
        {
            if (![document isLocked] || [document allowsPrinting])
            {
                [(NSTextField *)sub setEnabled:NO];
            }
            else
            {
                [(NSTextField *)sub setEnabled:YES];
                
                [(NSTextField *)sub setTarget:self];
                [(NSTextField *)sub setAction:@selector(pwButtonAction:)];
            }
        }
        else if ([sub.identifier isEqualToString:@"preView"])
        {
            if (![document isLocked] || [document allowsPrinting])
            {
                [(PDFView *)sub setDocument:document];
                [(PDFView *)sub setToolTip:@""];
            }
            else
            {
                [(PDFView *)sub setDocument:nil];
                [(PDFView *)sub setToolTip:NSLocalizedString(@"PasswordProtectedPDFViewTooltip", nil)];
            }
        }
        else if ([sub.identifier isEqualToString:@"stateImage"])
        {
            if ([document isLocked] || ![document allowsPrinting])
            {
                [(NSImageView *)sub setImage:[[NSBundle mainBundle] imageForResource:@"Warning"]];
                [(NSImageView *)sub setToolTip:NSLocalizedString(@"WarningTooltip", nil)];
            }
            else
            {
                [(NSImageView *)sub setImage:[[NSBundle mainBundle] imageForResource:@"Ok"]];
                [(NSImageView *)sub setToolTip:NSLocalizedString(@"OkTooltip", nil)];
            }
        }
    }
    
    [popover setAppearance:NSPopoverAppearanceMinimal];
    [popover setBehavior:NSPopoverBehaviorTransient];
    [popover showRelativeToRect:sender.superview.frame ofView:sender.superview preferredEdge:NSMinYEdge];
}

- (void)pwButtonAction:(NSButton *)sender
{
    NSInteger index = [self.table selectedRow];
    NSView * editView = [(AppDelegate *)[NSApp delegate] editView];
    PDFDocument * document = [self.documents objectAtIndex:index];
    
    for (NSView * sub in [editView subviews])
    {
        if ([sub.identifier isEqualToString:@"passwordField"])
        {
            [document unlockWithPassword:[(NSTextField *)sub stringValue]];
            
            if (![document isLocked] || [document allowsPrinting])
            {
                [(NSTextField *)sub setEnabled:NO];
            }
            else
            {
                [editView setAnimations:@{@"frameOrigin" : [(NSTextField *)sub shakeAnimation:editView.frame]}];
                [[editView animator] setFrameOrigin:editView.frame.origin];
            }
        }
    }
    
    NSTableCellView * view = [self.table viewAtColumn:0 row:index makeIfNecessary:NO];
    
    if ([document isLocked] || ![document allowsPrinting])
    {
        for (NSView * sub in [editView subviews])
        {
            if ([sub.identifier isEqualToString:@"stateImage"])
            {
                [(NSImageView *)sub setImage:[[NSBundle mainBundle] imageForResource:@"Warning"]];
                break;
            }
        }
        
        if (view)
        {
            for (NSView * sub in view.subviews)
            {
                if ([[sub identifier] isEqualToString:@"stateImage"])
                {
                    [(NSImageView *)sub setImage:[[NSBundle mainBundle] imageForResource:@"Warning"]];
                    break;
                }
            }
        }
    }
    else
    {
        [self _documentChangedNotification];
        
        for (NSView * sub in [editView subviews])
        {
            if ([sub.identifier isEqualToString:@"stateImage"])
            {
                [(NSImageView *)sub setImage:[[NSBundle mainBundle] imageForResource:@"Ok"]];
            }
            if ([sub.identifier isEqualToString:@"preView"] && (![document isLocked] || [document allowsPrinting]))
            {
                [(PDFView *)sub setDocument:document];
            }
        }
        
        if (view)
        {
            for (NSView * sub in view.subviews)
            {
                if ([[sub identifier] isEqualToString:@"stateImage"])
                {
                    [(NSImageView *)sub setImage:[[NSBundle mainBundle] imageForResource:@"Ok"]];
                    break;
                }
            }
        }
    }
}

#pragma mark *** Datasource ***

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    return [self.documents count];
}

- (NSDragOperation)tableView:(NSTableView *)tableView
                validateDrop:(id<NSDraggingInfo>)info
                 proposedRow:(NSInteger)row
       proposedDropOperation:(NSTableViewDropOperation)dropOperation
{
    if ([info draggingSource] == tableView)
	{
		[tableView setDropRow:row dropOperation:NSTableViewDropAbove];
        
		return NSDragOperationNone;
	}
	else
    {
		return NSDragOperationCopy;
	}
    
    return NSDragOperationNone;
}

- (BOOL)tableView:(NSTableView *)tableView
       acceptDrop:(id<NSDraggingInfo>)info
              row:(NSInteger)row
    dropOperation:(NSTableViewDropOperation)dropOperation
{
    BOOL acceptDrop = NO;
    
    if ([info draggingSource] == tableView)
    {
        return acceptDrop;
    }
    else
    {
        NSArray			* classes = [[NSArray alloc] initWithObjects:[NSURL class], nil];
        NSDictionary	* options = [NSDictionary dictionary];
        NSArray			* copiedItems = [[info draggingPasteboard] readObjectsForClasses:classes
                                                                           options:options];
        
        if(!copiedItems)
        {
            return acceptDrop;
        }
        
        NSMutableArray * docs = [[NSMutableArray alloc] initWithCapacity:copiedItems.count];
        NSMutableIndexSet * indexes = [[NSMutableIndexSet alloc] init];
        
        for (NSURL * url in copiedItems)
        {
            if ([[url pathExtension] isEqualToString:@"pdf"])
            {
                PDFDocument * document = [[PDFDocument alloc] initWithURL:url];
                
                [indexes addIndex:row + docs.count];
                [docs addObject:document];
            }
            else if ([[NSImage imageFileTypes] containsObject:[url pathExtension]])
            {
                PDFPage * page = [[PDFPage alloc] initWithImage:[[NSImage alloc] initWithContentsOfURL:url]];
                PDFDocument * document = [[PDFDocument alloc] init];
                
                [document insertPage:page atIndex:0];
                [document setDocumentAttributes:@{PDFDocumentTitleAttribute:
                                                      [[url lastPathComponent] stringByDeletingPathExtension]}];
                
                [indexes addIndex:row + docs.count];
                [docs addObject:document];
            }
        }
        
        [self insertDocuments:docs atIndexes:indexes];
        
        acceptDrop = YES;
    }
    
    return acceptDrop;
}

#pragma mark *** Delegate ***

- (NSView *)tableView:(NSTableView *)tableView
   viewForTableColumn:(NSTableColumn *)tableColumn
                  row:(NSInteger)row
{
    NSTableCellView * view = [tableView makeViewWithIdentifier:@"docView" owner:self];
    
    for (NSView * sub in view.subviews)
    {
        if ([[sub identifier] isEqualToString:@"docName"])
        {
            [(NSTextField *)sub setStringValue:[self.documents[row] documentName]];
        }
        else if ([[sub identifier] isEqualToString:@"stateImage"])
        {
            if ([self.documents[row] isLocked] || ![self.documents[row] allowsPrinting])
            {
                [(NSImageView *)sub setImage:[[NSBundle mainBundle] imageForResource:@"Warning"]];
                [(NSImageView *)sub setToolTip:NSLocalizedString(@"WarningTooltip", nil)];
            }
            else
            {
                [(NSImageView *)sub setImage:[[NSBundle mainBundle] imageForResource:@"Ok"]];
                [(NSImageView *)sub setToolTip:NSLocalizedString(@"OkTooltip", nil)];
            }
        }
        else if ([[sub identifier] isEqualToString:@"editButton"])
        {
            [(NSButton *)sub setTarget:self];
            [(NSButton *)sub setAction:@selector(editButtonAction:)];
            [(NSImageView *)sub setToolTip:NSLocalizedString(@"EditButtonTooltip", nil)];
        }
    }
    
    return view;
}

@end
