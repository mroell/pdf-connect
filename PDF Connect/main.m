//
//  main.m
//  PDF Connect
//
//  Created by Matthias Roell on 18.09.13.
//  Copyright (c) 2013 Matthias Roell. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
