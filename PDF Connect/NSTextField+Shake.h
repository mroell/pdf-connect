//
//  NSTextField+Shake.h
//  PDF Connect
//
//  Created by Matthias Röll on 24.09.13.
//  Copyright (c) 2013 Matthias Roell. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Quartz/Quartz.h>

@interface NSTextField (Shake)

- (CAKeyframeAnimation *)shakeAnimation:(NSRect)frame;

@end
