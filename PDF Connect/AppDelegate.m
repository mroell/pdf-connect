//
//  AppDelegate.m
//  PDF Connect
//
//  Created by Matthias Roell on 18.09.13.
//  Copyright (c) 2013 Matthias Roell. All rights reserved.
//

#import "AppDelegate.h"

#import "PDFModel.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    [self.window setBackgroundColor:[NSColor colorWithCalibratedWhite:0.95 alpha:1.0]];
    [self.previewPanel setBackgroundColor:[NSColor colorWithCalibratedWhite:0.95 alpha:1.0]];
    
    [[PDFModel sharedModel] setTable:self.tableView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshPreviewAction:)
                                                 name:PDFModelDocumentUpdatedNotification
                                               object:nil];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return YES;
}

- (IBAction)newAction:(id)sender
{
    [[PDFModel sharedModel] newDocument];
}

- (IBAction)saveAction:(id)sender
{
    NSSavePanel * panel = [NSSavePanel savePanel];
    
    [panel setAllowedFileTypes:@[@"pdf"]];
    [panel setAllowsOtherFileTypes:NO];
    //[panel setBackgroundColor:[NSColor colorWithCalibratedWhite:0.95 alpha:1.0]];
    
    [panel setMessage:NSLocalizedString(@"LargeFileWarning", nil)];
    
    [panel beginSheetModalForWindow:self.window completionHandler:^(NSInteger result)
     {
         if (result == NSFileHandlingPanelOKButton)
         {
             PDFDocument * document = [[PDFModel sharedModel] document];
             
             [document setDocumentAttributes:@{PDFDocumentAuthorAttribute        : NSFullUserName(),
                                               PDFDocumentCreationDateAttribute  : [NSDate date],
                                               PDFDocumentProducerAttribute      : @"PDF Connect"}];
             
             [[document dataRepresentation] writeToURL:panel.URL atomically:NO];
         }
     }];
}

- (IBAction)printAction:(id)sender
{
    [self previewAction:sender];
    [self.previewView print:sender];
}

- (IBAction)removeAction:(id)sender
{
    NSInteger index = [self.tableView selectedRow];
    
    if (index < 0)
    {
        NSBeep();
    }
    else
    {
        [[PDFModel sharedModel] removeObjectFromDocumentsAtIndex:index];
        [self.tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:index] byExtendingSelection:NO];
    }
}

- (IBAction)addAction:(id)sender
{
    NSOpenPanel * panel = [NSOpenPanel openPanel];
    
    [panel setAllowsMultipleSelection:YES];
    [panel setAllowedFileTypes:[[NSImage imageFileTypes] arrayByAddingObject:@"pdf"]];
    [panel setAllowsOtherFileTypes:NO];
    [panel setCanChooseDirectories:NO];
    //[panel setBackgroundColor:[NSColor colorWithCalibratedWhite:0.95 alpha:1.0]];
    
    [panel beginSheetModalForWindow:self.window completionHandler:^(NSInteger result)
     {
         if (result == NSFileHandlingPanelOKButton)
         {
             NSMutableSet * docs = [[NSMutableSet alloc] initWithCapacity:panel.URLs.count];
             
             for (NSURL * url in [panel URLs])
             {
                 if ([[url pathExtension] isEqualToString:@"pdf"])
                 {
                     PDFDocument * document = [[PDFDocument alloc] initWithURL:url];
                     [docs addObject:document];
                 }
                 else if ([[NSImage imageFileTypes] containsObject:[url pathExtension]])
                 {
                     PDFPage * page = [[PDFPage alloc] initWithImage:[[NSImage alloc] initWithContentsOfURL:url]];
                     PDFDocument * document = [[PDFDocument alloc] init];
                     
                     [document insertPage:page atIndex:0];
                     [document setDocumentAttributes:@{PDFDocumentTitleAttribute:
                                                           [[url lastPathComponent] stringByDeletingPathExtension]}];
                     
                     [docs addObject:document];
                 }
             }
             
             [[PDFModel sharedModel] addDocuments:docs];
         }
     }];
}

- (IBAction)moveDownAction:(id)sender
{
    NSInteger index = [self.tableView selectedRow];
    
    if (index == [[[PDFModel sharedModel] documents] count] - 1)
    {
        NSBeep();
    }
    else if (index >= 0)
    {
        PDFDocument * document = [[[PDFModel sharedModel] documents] objectAtIndex:index];
        
        [[[PDFModel sharedModel] documents] removeObject:document];
        [[PDFModel sharedModel] insertObject:document inDocumentsAtIndex:index + 1];
        [self.tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:index + 1] byExtendingSelection:NO];
    }
}

- (IBAction)moveUpAction:(id)sender
{
    NSInteger index = [self.tableView selectedRow];
    
    if (index == 0)
    {
        NSBeep();
    }
    else if (index > 0)
    {
        PDFDocument * document = [[[PDFModel sharedModel] documents] objectAtIndex:index];
        
        [[[PDFModel sharedModel] documents] removeObject:document];
        [[PDFModel sharedModel] insertObject:document inDocumentsAtIndex:index - 1];
        [self.tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:index - 1] byExtendingSelection:NO];
    }
}

- (IBAction)previewAction:(id)sender
{
    [self.previewPanel makeKeyAndOrderFront:sender];
    [self refreshPreviewAction:sender];
}

- (IBAction)refreshPreviewAction:(id)sender
{
    [self.previewView setDocument:[[PDFModel sharedModel] document]];
}

@end
