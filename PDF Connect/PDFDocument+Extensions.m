//
//  PDFDocument+Extensions.m
//  PDF Connect
//
//  Created by Matthias Roell on 18.09.13.
//  Copyright (c) 2013 Matthias Roell. All rights reserved.
//

#import "PDFDocument+Extensions.h"

@implementation PDFDocument (Extensions)

- (NSString *)documentName
{
    NSString * name = [[self.documentURL lastPathComponent] stringByDeletingPathExtension];
    
    if (!name)
    {
        name = [[self documentAttributes] valueForKey:PDFDocumentTitleAttribute];
    }
    
    if (!name)
    {
        return @"";
    }
    
    return name;
}

@end
