//
//  AppDelegate.h
//  PDF Connect
//
//  Created by Matthias Roell on 18.09.13.
//  Copyright (c) 2013 Matthias Roell. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#include <Quartz/Quartz.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSTableView *tableView;

@property (unsafe_unretained) IBOutlet NSPanel *previewPanel;
@property (weak) IBOutlet PDFView *previewView;

@property (weak) IBOutlet NSView *editView;
@property (weak) IBOutlet NSPopover *editPopover;

- (IBAction)newAction:(id)sender;

- (IBAction)saveAction:(id)sender;
- (IBAction)printAction:(id)sender;

- (IBAction)removeAction:(id)sender;
- (IBAction)addAction:(id)sender;

- (IBAction)moveDownAction:(id)sender;
- (IBAction)moveUpAction:(id)sender;

- (IBAction)previewAction:(id)sender;

- (IBAction)refreshPreviewAction:(id)sender;

@end
